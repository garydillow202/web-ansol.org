---
categories: []
metadata:
  event_location:
  - event_location_value: Lisboa
  event_site:
  - event_site_url: http://ec.europa.eu/portugal/comissao/destaques/20150312_ttip_dialogo_parceiros_pt.htm
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2015-03-26 00:00:00.000000000 +00:00
    event_start_value2: 2015-03-27 00:00:00.000000000 +00:00
  slide:
  - slide_value: 0
  node_id: 294
layout: evento
title: 'TTIP: diálogo entre parceiros'
created: 1426540610
date: 2015-03-16
---
<p>O evento pode também ser acompanhado online, em https://twitter.com/search?f=realtime&amp;q=%23dialogoTTIP&amp;src=tyah .</p>
