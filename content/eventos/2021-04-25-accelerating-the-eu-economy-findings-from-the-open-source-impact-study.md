---
categories: []
metadata:
  mapa:
  - {}
  slide:
  - slide_value: 0
  node_id: 797
  event:
    location: Online
    site:
      title: ''
      url: https://www.crowdcast.io/e/accelerating-the-eu-economy/register
    date:
      start: 2021-05-20 00:00:00.000000000 +01:00
      finish: 2021-05-20 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: 'Accelerating the EU economy: Findings from the Open Source Impact Study'
created: 1619376807
date: 2021-04-25
aliases:
- "/evento/797/"
- "/node/797/"
---

