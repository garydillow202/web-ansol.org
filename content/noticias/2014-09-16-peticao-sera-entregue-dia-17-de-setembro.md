---
excerpt: A ANSOL (Associação Nacional para o Software Livre) opõe-se à proposta de
  lei 246/XII e apresentará amanhã, dia 17 de Setembro, a petição que lançou contra
  a proposta quando passar dos 4000 subscritores, requisito para apreciação em Plenário
  da Assembleia da República.
categories:
- "#pl246"
- cópia privada
- press release
- imprensa
metadata:
  tags:
  - tags_tid: 45
  - tags_tid: 11
  - tags_tid: 9
  - tags_tid: 19
  node_id: 221
layout: article
title: Petição será entregue dia 17 de Setembro
created: 1410867020
date: 2014-09-16
aliases:
- "/article/221/"
- "/node/221/"
- "/pr-20140916/"
---
<p>Lisboa, 16 de Setembro de 2014 - A ANSOL (Associação Nacional para o Software Livre) opõe-se à proposta de lei 246/XII e apresentará amanhã, dia 17 de Setembro, a petição que lançou contra a proposta quando passar dos 4000 subscritores, requisito para apreciação em Plenário da Assembleia da República.</p><p>Rui Seabra, presidente da Direção da ANSOL informa que «<em>a petição conta já com quase 3000 assinaturas passado e ainda nem um dia tem!</em>» tornando provável que se chegue aos valores requeridos pela AR durante o dia de hoje.</p><p>A petição está disponível em <a href="http://bit.do/pl246">http://bit.do/pl246</a> apelando entre outros aos deputados da AR para que não aprovem a proposta, citando entre outros motivos a ausência de prejuízo para os autores, uma das conclusões do <a href="http://ec.europa.eu/internal_market/copyright/docs/studies/140623-limitations-economic-impacts-study_en.pdf" title="Assessing the economic impacts of adapting certain limitations and  exceptions to copyright and related rights in the EU – Analysis of specific policy options">estudo da Comissão Europeia sobre a matéria</a> que concluiu também haver maiores efeitos positivos para todas as partes acabando com as taxas sobre a cópia privada.</p><p>«<em>Contamos que os portugueses se façam ouvir para evitar taxas injustas que beneficiam entidades obsoletas, e que o Governo promova uma verdadeira reforma do direito de autor na Europa de forma aberta à participação dos cidadãos, que re-equilibre os direitos de todas as partes antes de legislar em benefício de algumas</em>» conclui Rui Seabra.</p><p>&nbsp;</p>
