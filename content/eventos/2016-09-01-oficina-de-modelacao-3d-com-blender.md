---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 447
  event:
    location: Águeda
    site:
      title: ''
      url: https://docs.google.com/forms/d/e/1FAIpQLSclEf8noo_auoeqMg6WGAdh5IM5Wu0mmgKkGONRhnFIkImlaw/viewform
    date:
      start: 2016-09-23 00:00:00.000000000 +01:00
      finish: 2016-09-23 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: Oficina de Modelação 3D com Blender
created: 1472740026
date: 2016-09-01
aliases:
- "/evento/447/"
- "/node/447/"
---
<p>Destinatários &gt; 16 anos</p>
