---
categories:
- floss
- open tech
- meeting
metadata:
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 273
  - tags_tid: 282
  - tags_tid: 129
  node_id: 595
  event:
    location: Sheffield, UK
    site:
      title: Oggcamp
      url: http://oggcamp.org/
    date:
      start: 2018-08-18 00:00:00.000000000 +01:00
      finish: 2018-08-19 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: OggCamp 2018
created: 1522934027
date: 2018-04-05
aliases:
- "/evento/595/"
- "/node/595/"
---
<p>OggCamp is an unconference celebrating Free Culture, Free and Open Source Software, hardware hacking, digital rights, and all manner of collaborative cultural activites. You can find out what happens at an OggCamp by watching the video below:</p><p>https://youtu.be/K15PIGuiLKw<br><br></p><p><br> <br><br><br></p>
