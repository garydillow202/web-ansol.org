---
categories: []
metadata:
  event_site:
  - event_site_url: http://www.festadoavante.pcp.pt/2012/
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2012-09-06 23:00:00.000000000 +01:00
    event_start_value2: 2012-09-08 23:00:00.000000000 +01:00
  node_id: 84
layout: evento
title: Festa do Avante!
created: 1347207553
date: 2012-09-09
---
<p>Na Festa do Avante! de 2012, existem v&aacute;rios sub-eventos relacionados com Software Livre:</p>
<p><strong>No Audit&oacute;rio de Debates do Pavilh&atilde;o Central:</strong></p>
<ul>
	<li>
		No S&aacute;bado &agrave;s 18h00, &quot;Tecnologias Abertas ao servi&ccedil;o do Povo e do Pa&iacute;s&quot;, com Bruno Dias e Nuno Boavida;</li>
	<li>
		No Domingo &agrave;s 15h30 &quot;A partilha do conhecimento no dom&iacute;nio digital&quot;, com Miguel Tiago e Gon&ccedil;alo Valverde;</li>
</ul>
<p><strong>Apresenta&ccedil;&atilde;o do Komunix v9.0:</strong></p>
<p>Baseado no Lubuntu 12.04, uma variante do Ubuntu que utiliza o ambiente de &aacute;rea de trabalho The Lightweight X11 Desktop Environment, tamb&eacute;m conhecido como LXDE. Tem como caracter&iacute;sticas principais, um ambiente leve, r&aacute;pido e com economia de energia, sendo geralmente recomendado para uso em computadores com especifica&ccedil;&otilde;es baixas.&nbsp; Disponibilizado em LiveCD (Um CD que quando colocado no computador durante a inicializa&ccedil;&atilde;o, arranca com um sistema operativo completo, sem a necessidade de altera&ccedil;&otilde;es no computador). Tem a possibilidade de ser instalado no computador.</p>
