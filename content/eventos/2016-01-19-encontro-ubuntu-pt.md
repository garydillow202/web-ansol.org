---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 392
  event:
    location: Sintra
    site:
      title: ''
      url: https://www.facebook.com/events/543961025769936/
    date:
      start: 2016-01-21 00:00:00.000000000 +00:00
      finish: 2016-01-21 00:00:00.000000000 +00:00
    map: {}
layout: evento
title: Encontro Ubuntu-pt
created: 1453211935
date: 2016-01-19
aliases:
- "/evento/392/"
- "/node/392/"
---
<div id="event_description" data-referrer="event_description"><div class="_4-u2 _36i5 _4-u8"><div class="_1w2q"><span class="fsl">No próximo dia 21 de Janeiro pelas 20.00 realiza-se mais um encontro da comunidade Ubuntu Portugal no Saloon, em Sintra.<br> <br> Vem, traz um amigo ou um familiar e vem conviver e partilhar experiências com o resto da comunidade portuguesa.<br> <br> Deixamos um mapa com a localização do «Saloon» (marcado como «Sala de Estar»)<br> <a href="http://www.openstreetmap.org/node/1594158358" target="_blank" rel="nofollow nofollow"><span>http://</span><span>www.openstreetmap.org/node/</span>1594158358</a><br> <br> Mais Informações:<br> Saloon<br> Av. Movimento das Forças Armadas, 5 - Sintra<br> (2 min a pé da estaçao de comboios da portela de Sintra)<br> O wi-fi é grátis.</span></div></div></div>
