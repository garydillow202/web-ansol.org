---
categories:
- censura
- savecodeshare
- artigo 13
- copyright
- keepsharing
- direitos de autor
- direito de autor
- direitos digitais
metadata:
  tags:
  - tags_tid: 216
  - tags_tid: 199
  - tags_tid: 217
  - tags_tid: 194
  - tags_tid: 218
  - tags_tid: 96
  - tags_tid: 43
  - tags_tid: 113
  node_id: 550
layout: article
title: Diz não à Censura Online na Europa!
created: 1520511772
date: 2018-03-08
aliases:
- "/article/550/"
- "/node/550/"
---
<iframe src="https://www.youtube-nocookie.com/embed/ZMh8qPAR6aM"
        width="560" height="315"
        title="YouTube video player"
        frameborder="0"
        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
        allowfullscreen></iframe>

Foi ontem lançada mais uma campanha, a nível europeu, contra a proposta que se
encontra em debate a nível europeu, e apoiada pelo Governo Português, para a
obrigatoriedade de implementação de filtros nas plataformas online que permitem
a publicação de conteúdos.

Esta campanha, que pode ser vista [aqui][campanha], pede a todos vós um pequeno
momento do vosso tempo. Preenchendo um formulário, uma carta também em vosso
nome será enviada aos membros do Parlamento Europeu, pedindo que rejeitem esta
medida, assim que se obtenham 2500 assinaturas.

A ANSOL apoia a campanha, dizendo o seu Presidente sobre o tópico:

> A forma como o Software Livre é desenvolvido tem sido, desde há muito, um
> esforço comunitário potenciado pela comunicação rápida que a Internet
> facilita. Pedaços de código, com pequenas alterações, correcções ou
> melhorias, são tornadas públicas para que outros as possam usar, partilhar, e
> trabalhar sobre elas. Milhões destes uploads acontecem diariamente, e a
> existência de cada um deles é agora posta em risco com a proposta da
> colocação de uma máquina de censura no caminho entre a criação e a
> publicação.

Já no passado mês de Fevereiro, a ANSOL juntou-se a oito outras entidades
Portuguesas, enviando uma [carta aberta][d3] ao Governo Português, pedindo-lhe
para rever a sua posição sobre esta matéria. A carta aguarda ainda resposta.

[campanha]: https://www.liberties.eu/en/campaigns/protect-free-speech-campaign-online-censorship/249
[d3]: https://direitosdigitais.pt/comunicacao/25-comunicados-de-imprensa/41-carta-aberta-sobre-a-posicao-do-governo-portugues-no-debate-relativo-a-diretiva-europeia-de-copyright
