---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 403
  event:
    location: Rua Cimo do Telheiro, 47, 4440-452 São Mamede De Infesta
    site:
      title: Genuino Day 2016
      url: http://labcd.org/index.php?task=evt&id=138&option=com_lcd&Itemid=2
    date:
      start: 2016-04-02 14:00:00.000000000 +01:00
      finish: 2016-04-02 19:00:00.000000000 +01:00
    map: {}
layout: evento
title: Genuino Day 2016
created: 1456946987
date: 2016-03-02
aliases:
- "/evento/403/"
- "/node/403/"
---
<p>O <a href="http://labcd.org/" target="_blank">Laboratório de Criação Digital</a> vai participar no Genuino Day 2016.<br><br>Este dia é uma celebração do aniversário do Arduino/Genuino e do movimento Open Electronics organizado pela comunidade.<br><br>É um evento gratuito e de acesso livre que procura dar a conhecer o Arduino/Genuino e o conceito de Open Electronics ao público em geral, mas que também procura ser um ponto de encontro dos já envolvidos.<br><br>Vamos ter demonstrações de alguns equipamentos e vamos estar disponíveis para explicar como é que se montaram e programaram.<br><br>Tens algo de interessante para mostrar? Traz e rapidamente se arranjará um espaço para poderes mostrar.<br><br>Acima de tudo procuramos proporcionar um dia divertido que propricie a troca de conhecimentos e experiências.</p>
