---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 336
  event:
    location: Porto
    site:
      title: ''
      url: http://www.meetup.com/WP-Porto/events/223986352/
    date:
      start: 2015-07-30 19:00:00.000000000 +01:00
      finish: 2015-07-30 21:00:00.000000000 +01:00
    map: {}
layout: evento
title: 'Design para Wordpress: Comprar ou Criar?'
created: 1437429783
date: 2015-07-20
aliases:
- "/evento/336/"
- "/node/336/"
---

