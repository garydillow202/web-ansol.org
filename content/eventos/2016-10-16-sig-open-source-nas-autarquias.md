---
categories:
- gis
- osgeo pt
- osgeo
- autarquias
metadata:
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 180
  - tags_tid: 181
  - tags_tid: 182
  - tags_tid: 183
  node_id: 467
  event:
    location: Albergaria-a-Velha (Biblioteca Municipal)
    site:
      title: ''
      url: http://osgeopt.pt/meetup/
    date:
      start: 2016-10-27 00:00:00.000000000 +01:00
      finish: 2016-10-27 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: SIG Open-Source nas Autarquias
created: 1476629220
date: 2016-10-16
aliases:
- "/evento/467/"
- "/node/467/"
---

