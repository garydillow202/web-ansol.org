---
categories: []
metadata:
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 38
  node_id: 209
layout: page
title: 30 Anos de GNU
created: 1378490456
date: 2013-09-06
---
<p><a href="https://gnu.org/gnu30"><img src="https://static.fsf.org/nosvn/misc/GNU_30th_badge.png" alt="[ Celebrate 30 years of GNU! ]" style="display: block; margin-left: auto; margin-right: auto;" height="145" width="145"></a></p>
