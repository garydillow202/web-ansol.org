---
layout: evento
title: OFE Community Meeting 2022
metadata:
  event:
    date:
      start: 2022-09-30
      finish: 2022-09-30
    location: online
    site:
      url: https://openforumeurope.org/event/ofe-community-meeting-2022/
---


