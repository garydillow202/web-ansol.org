---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 380
  event:
    location: Casa das Artes da Fundação Bissaya Barreto, Coimbra, Portugal
    site:
      title: 
      url: 
    date:
      start: 2015-11-20 00:00:00.000000000 +00:00
      finish: 2015-11-22 00:00:00.000000000 +00:00
    map: {}
layout: evento
title: Encon­tro de Labo­ra­tó­rios Audi­ên­cia Zero
created: 1447020894
date: 2015-11-08
aliases:
- "/evento/380/"
- "/node/380/"
---
Desde 2009 que os labo­ra­tó­rios da rede AZ Labs: altLab (Lis­boa), LCD (Porto) e xDA (Coim­bra), orga­ni­zam o seu encon­tro anual em mea­dos de Dezem­bro. Nes­tes encon­tros reunem-se os mem­bros dos vários labo­ra­tó­rios da 
Audi­ên­cia Zero para mos­trar o seu tra­ba­lho e par­ti­lhar expe­ri­ên­cias de uma forma aberta e informal.

Com a pro­li­fe­ra­ção recente de espa­ços que ligam a cri­a­ti­vi­dade à tec­no­lo­gia como hac­kers­pa­ces, makerspa­ces, fablabs e medialabs, nota-se um inte­resse gene­ra­li­zado da soci­e­dade com o o empower­ment tec­no­ló­gico 
e cri­a­tivo do indi­vi­duo e com a cri­a­ção de comu­ni­da­des autó­no­mas de par­ti­lha livre de conhe­ci­mento. Torna-se assim para nós muito impor­tante par­ti­lhar com a soci­e­dade civil o tra­ba­lho e a expe­ri­ên­cia de campo 
adqui­rida ao longo dos anos pelos Labo­ra­tó­rios Audi­ên­cia Zero e a aber­tura da Rede atra­vés da cri­a­ção de novos laços com pes­soas, espa­ços e colec­ti­vos que par­ti­lham dos valo­res da associação.

Este ano o Encon­tro de Labo­ra­tó­rios Audi­ên­cia Zero irá decor­rer em Coimbra no CondomínioCriativo entre os dias 20 e 22 de Novembro de 2015, na Casa das Artes da Fundação Bissaya Barreto.

Con­vi­da­mos assim todos os mem­bros e ami­gos da nossa comu­ni­dade a par­ti­ci­pa­rem neste evento atra­vés da pro­posta de acti­vi­da­des a rea­li­zar durante o encon­tro. As acti­vi­da­des pro­pos­tas podem ser de qual­quer tipo, desde que de alguma forma se ade­quem aos temas da apro­pri­a­ção cri­a­tiva da tec­no­lo­gia e da par­ti­lha livre de 
conhe­ci­men­tos, como por exemplo:

- Talks sobre um tema à vossa esco­lha com uma dura­ção até 30 minutos.
- Workshops com uma dura­ção máximo de duas horas que visam que­brar o gelo e intro­du­zir o público às tec­no­lo­gias do dia-a-dia dos labo­ra­tó­rios. Exem­plos: impres­são 3D, Arduino, cir­cui­tos elec­tró­ni­cos, fabri­ca­ção de 
PCB, sin­tese de audio, etc.
- Mesas de tra­ba­lho temá­ti­cas onde os par­ti­ci­pan­tes se reu­nem e tra­ba­lhem em pro­jec­tos durante todo o evento. As mesas de tra­ba­lho podem ser orga­ni­za­das à volta de pro­jec­tos con­cre­tos já em desen­vol­vi­mento ou de ideias aber­tas à dis­cus­são e par­ti­ci­pa­ção de todos.
- Demons­tra­ções de pro­jec­tos desen­vol­vi­dos pelos participantes.
- Per­for­man­ces, con­cer­tos, ses­sões de DJ e VJ.

Os inte­res­sa­dos deve­rão enviar até ao pró­ximo dia 15 de Novembro uma breve des­cri­ção da acti­vi­dade que pre­ten­dem rea­li­zar, junto com uma lista dos recur­sos neces­sá­rios para o email <a href="mailto:serratiago@gmail.com" class="moz-txt-link-abbreviated">serratiago@gmail.com</a> .<a href="mailto:serratiago@gmail.com" class="moz-txt-link-rfc2396E"><br></a>
