---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 269
  event:
    location: 
    site:
      title: ''
      url: http://www.europarl.europa.eu/sides/getDoc.do?pubRef=-%2f%2fEP%2f%2fTEXT%2bCOMPARL%2bIMCO-OJ-20150205-1%2b02%2bDOC%2bXML%2bV0%2f%2fEN&language=EN
    date:
      start: 2015-02-25 00:00:00.000000000 +00:00
      finish: 2015-02-25 00:00:00.000000000 +00:00
    map: {}
layout: evento
title: 'TTIP: Deadline for tabling amendments'
created: 1422370562
date: 2015-01-27
aliases:
- "/evento/269/"
- "/node/269/"
---

