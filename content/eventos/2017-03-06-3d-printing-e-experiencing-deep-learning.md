---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 491
  event:
    location: Lisboa
    site:
      title: ''
      url: https://www.meetup.com/Big-Data-Developers-in-Lisbon/events/237672655/
    date:
      start: 2017-03-08 19:00:00.000000000 +00:00
      finish: 2017-03-08 21:30:00.000000000 +00:00
    map: {}
layout: evento
title: 3D Printing e Experiencing Deep Learning
created: 1488834799
date: 2017-03-06
aliases:
- "/evento/491/"
- "/node/491/"
---
<p>Um evento com duas sessões: "3D Printing : A case study of Streaming Data Analytics", e "Experiencing Deep Learning".</p>
