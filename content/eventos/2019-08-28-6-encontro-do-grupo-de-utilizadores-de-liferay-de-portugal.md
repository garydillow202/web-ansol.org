---
categories: []
metadata:
  mapa:
  - mapa_geom: "\x01\a\0\0\0\0\0\0\0"
    mapa_geo_type: geometrycollection
    mapa_geohash: ''
  slide:
  - slide_value: 0
  node_id: 691
  event:
    location: 
    site:
      title: ''
      url: https://www.meetup.com/Liferay-Portugal-User-Group/events/261702314/
    date:
      start: 2019-09-18 00:00:00.000000000 +01:00
      finish: 2019-09-18 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: 6º Encontro do Grupo de Utilizadores de Liferay de Portugal
created: 1567011287
date: 2019-08-28
aliases:
- "/evento/691/"
- "/node/691/"
---

