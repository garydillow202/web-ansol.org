---
categories:
- software livre
- encontro
- polónia
metadata:
  event_location:
  - event_location_value: 'Poznan University of Technology Conference Center, ul.
      Piotrowo 3, Poznan, Poland '
  event_site:
  - event_site_url: https://piwo.informatyka.org/
    event_site_title: P.I.W.O.
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2018-04-27 23:00:00.000000000 +01:00
    event_start_value2: 2018-04-27 23:00:00.000000000 +01:00
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 41
  - tags_tid: 74
  - tags_tid: 253
  node_id: 573
layout: evento
title: P.I.W.O. - XIII EDYCJA
created: 1522854687
date: 2018-04-04
---
<p><a href="http://piwo.informatyka.org" class="http">PIWO</a> is one of the most popular polish FLOSS events. For P.I.W.O organizers, it’s always been about mobilizing creative individuals and promoting innovative IT ideas, developed with open source philosophy. Since the first edition of P.I.W.O in 2004, the conference hosted many speakers associated with free software movement, among whom linux enthusiasts, hackers, web specialists and Open Hardware fans.</p>
