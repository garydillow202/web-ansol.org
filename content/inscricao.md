---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 18
layout: page
title: Inscrição na ANSOL
created: 1333266103
date: 2012-04-01
aliases:
- "/node/18/"
- "/page/18/"
---
Pode inscrever-se na associação qualquer pessoa (portuguesa ou estrangeira,
moradora em Portugal ou no estrangeiro) que possa contribuir para a prossecução
dos objectivos da Associação. Não são aceites inscrições de empresas, mas uma ou
várias pessoas da empresa podem fazer a sua inscrição individual.

O primeiro passo para a inscrição consiste em tomar conhecimento e aceitar os <a href="{{< ref "estatutos" >}}">estatutos</a>.
O <a href="{{< ref "regulamento-interno" >}}">regulamento interno</a> também deverá ser lido e aceite.

A jóia de inscrição é actualmente inexistente (0 euro), e a quota anual 30
euros (6 euros no caso de estudantes, desempregados e reformados, mediante apresentação de comprovativo).

O pagamento da quota pode ser feito pessoalmente, por cheque, ou transferência bancária para o IBAN **PT50&nbsp;0035&nbsp;2178&nbsp;00027478430&nbsp;14**.

A inscrição é feita através do preenchimento do seguinte formulário:

<style>
  .control-group {
    display: flex;
    flex-wrap: wrap;
    gap: 10px;
    margin-bottom: 20px;
  }

  .control-label { min-width: 160px; }
  .controls { display: flex; flex-wrap: wrap; gap: 10px; }


</style>
<form method="post" name="inscricaosocio" action="https://form.ansol.org/forms/inscricao_socios_form.php" class="form-horizontal">
  <!-- Text input-->
  <div class="control-group">
    <label class="control-label" for="nome_proprio">Primeiro Nome</label>
    <div class="controls">
      <input id="nome_proprio" name="nome_proprio" type="text" placeholder="O seu primeiro nome" class="input-large" required>
    </div>
  </div>

  <!-- Text input-->
  <div class="control-group">
    <label class="control-label" for="apelido">Apelido</label>
    <div class="controls">
      <input id="apelido" name="apelido" type="text" placeholder="O seu apelido" class="input-large" required>
    </div>
  </div>

  <!-- Text input-->
  <div class="control-group">
    <label class="control-label" for="nome_proprio">Email</label>
    <div class="controls">
      <input id="email" name="email" type="email" placeholder="O seu email" class="input-large" required>
    </div>
  </div>

  <!-- Text input-->
  <div class="control-group">
    <label class="control-label" for="bicc">BI/Cartão Cidadão</label>
    <div class="controls">
      <input id="bicc" name="bicc" type="text" placeholder="Nº de BI ou Cartão de Cidadão" class="input-large" required>
    </div>
  </div>

  <!-- Text input-->
  <div class="control-group">
    <label class="control-label" for="morada">Morada</label>
    <div class="controls">
      <input id="morada" name="morada" type="text" placeholder="A sua morada completa" class="input-xxlarge" required>
    </div>
  </div>

  <!-- Text input-->
  <div class="control-group">
    <label class="control-label" for="cp">Código Postal</label>
    <div class="controls">
      <input id="cp" name="cp" type="text" placeholder="0000-000" class="input-mini" required>
      <input id="localidade" name="localidade" type="text" placeholder="Localidade" class="input-medium" required>
    </div>
  </div>

  <!-- Multiple Checkboxes (inline) -->
  <div class="control-group">
    <label class="checkbox inline" for="maillist-0">
      <input type="checkbox" name="maillist" id="maillist-0" value="newsletter">
      Pretendo receber newsletters e outras comunicações para sócios
      (Utilizada para contacto entre a ANSOL e os seus sócios, assim como entre estes.)
    </label>
  </div>

  <!-- Multiple Checkboxes -->
  <div class="control-group">
    <label class="checkbox" for="statement-0">
      <input type="checkbox" name="statement" id="statement-0" value="Declaro ter conhecimento e cumprir os Estatutos e o Regulamento Interno da ANSOL" required>
      Declaro ter conhecimento e cumprir os <a href="{{< ref "estatutos" >}}" target="_blank">Estatutos</a> e o <a href="{{< ref "regulamento-interno" >}}" target="_blank">Regulamento Interno</a> da ANSOL
    </label>
  </div>

  <!-- Button -->
  <div class="control-group">
    <label class="control-label" for="submit"></label>
    <div class="controls">
      <button id="submit" name="submit" class="cta">Submeter Inscrição</button>
    </div>
  </div>
</form>

A direcção da ANSOL reúne uma vez por semana para aprovar novas inscrições. Podemos demorar cerca de uma semana a responder ao teu pedido.

<span style="font-size: 13.008px; line-height: 1.538em;"><span style="font-size: 13.008px; line-height: 1.538em;">Após aprovação por parte da direcção, o candidato deve pagar o valor da jóia e da quota, passando a ser sócio a partir do momento em que a ANSOL receba o comprovativo de pagamento. Este comprovativo deve ser enviado num email para&nbsp;</span><a href="mailto:direccao@ansol.org">direccao@ansol.org</a><span style="font-size: 13.008px; line-height: 1.538em;">.</span></span>
