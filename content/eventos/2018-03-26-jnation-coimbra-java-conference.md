---
categories:
- java conference
- coimbra
metadata:
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 232
  - tags_tid: 233
  node_id: 560
  event:
    location: Convento de São Francisco, Coimbra
    site:
      title: JNation Conference
      url: https://jnation.pt
    date:
      start: 2018-06-19 00:00:00.000000000 +01:00
      finish: 2018-06-19 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: JNation Coimbra - Java Conference
created: 1522019394
date: 2018-03-26
aliases:
- "/evento/560/"
- "/node/560/"
---
<div class="wpb_column col-sm-6" style="height: 335px;"><div class="vc_column-inner "><div class="wpb_wrapper"><div class="wpb_text_column wpb_content_element "><div class="wpb_wrapper"><h2>The JNation Conference is about bringing together technology enthusiasts from all over the world with a passion for Java.</h2><p>For a full day, you will get to hear Rock Star speakers talk about Java and JVM related technologies, frameworks, tools, programming languages, cloud, internet of things and many more.</p><p>Join us in Coimbra, Portugal to share the knowledge and meet other enthusiasts in the Java Community.</p></div></div></div></div></div>
