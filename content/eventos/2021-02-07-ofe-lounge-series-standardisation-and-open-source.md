---
categories: []
metadata:
  mapa:
  - {}
  slide:
  - slide_value: 0
  node_id: 776
  event:
    location: Online
    site:
      title: ''
      url: https://openforumeurope.org/event/ofe-lounge-series-standardisation-and-open-source/
    date:
      start: 2021-02-11 17:00:00.000000000 +00:00
      finish: 2021-02-11 18:00:00.000000000 +00:00
    map: {}
layout: evento
title: 'OFE Lounge Series: Standardisation and Open Source'
created: 1612728853
date: 2021-02-07
aliases:
- "/evento/776/"
- "/node/776/"
---

