---
categories: []
metadata:
  anexos:
  - anexos_fid: 22
    anexos_display: 1
    anexos_description: ''
    anexos_uri: public://flyer1.png
  slide:
  - slide_value: 0
  node_id: 141
layout: page
title: 15 de Outubro
created: 1366399219
date: 2013-04-19
---
<p>A ANSOL foi subcritora da <a href="http://www.15deoutubro.net">Plataforma 15 de Outubro</a>.</p><p>&nbsp;</p><p>Entre outras coisas, participou em eventos, estando presente e distribuindo uma centena de CDs e vários milhares de flyers.</p><p>&nbsp;</p><p>Um exemplo de um dos flyers distribuídos pode ser visto aqui: <img src="https://ansol.org/sites/ansol.org/files/flyer1.png" style="width: 90%; display: block; margin-left: auto; margin-right: auto;"></p>
