---
categories: []
metadata:
  mapa:
  - {}
  slide:
  - slide_value: 0
  node_id: 799
  event:
    location: Online
    site:
      title: ''
      url: https://ec.europa.eu/eusurvey/runner/OSORCommunityEvent-Sustainability
    date:
      start: 2021-05-18 00:00:00.000000000 +01:00
      finish: 2021-05-18 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: Sustainability of Open Source Communities
created: 1620853375
date: 2021-05-12
aliases:
- "/evento/799/"
- "/node/799/"
---

