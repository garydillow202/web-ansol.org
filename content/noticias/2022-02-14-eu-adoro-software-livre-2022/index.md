---
categories:
- ilovefs
layout: article
title: Eu adoro Software Livre 2022
date: 2022-02-14
aliases:
- "/ilovefs2022/"
---
Todos os anos a [14 de Fevereiro][ilovefs], a ANSOL junta-se à [Free Software
Foundation Europe][fsfe] e pede a todos os utilizadores de Software Livre que
pensem naquelas pessoas que trabalham para a comunidade de Software Livre e mostrem
o seu apreço individualmente neste dia de "Eu adoro Software Livre".

Empresas, comunidades, projectos, associações e vários outros organismos têm
usado este dia para mostrar o seu agradecimento público àqueles que fazem o
mundo do Software Livre tão vibrante como é. Assim, também a ANSOL decide
aproveitar este dia para fazer alguns agradecimentos públicos.

A ANSOL [celebrou o seu vigésimo aniversário em 2021][20anos], e para festejar
organizou um ciclo de apresentações online, tirando proveito de várias soluções
de software livre:

* [**Owncast**](https://owncast.online/), uma solução de transmissão de vídeo
  em directo, onde a audiência assistiu via web às apresentações e onde pôde
  interagir com os/as oradores/as via texto;
* [**VDO.Ninja**](https://vdo.ninja/), uma ferramenta que permite integrar e
  coordenar várias câmaras. Permitiu, enquanto uma apresentação decorria, à
  próxima pessoa que se preparasse e testasse o áudio, vídeo, e partilha de
  ecrã;
* [**OBS**](https://obsproject.com/), um programa usado para misturar as várias
  câmaras, compor as cenas, e aplicar transições e ecrãs de espera entre as
  sessões;
* [**Jitsi**](https://jitsi.org/), uma plataforma de videoconferências
  utilizada durante a mesa redonda com os antigos presidentes da ANSOL.


As operações diárias da ANSOL também dependem de vários componentes de Software
Livre. Ficam aqui algumas menções a tecnologias adoptadas no último ano:

* [**Hugo**](https://gohugo.io/), uma _framework_ de construção de websites
  estáticos, que está neste momento a ser usada para o site institucional da
  associação;
* [**Matrix**](https://matrix.org/), uma rede de comunicação descentralizada,
  usada agora para alojar as salas da comunidade e de trabalho da ANSOL;
* [**Jitsi**](https://jitsi.org), plataforma com direito a menção dupla, por
  também ser usada para as reuniões semanais da direcção.

Com a continuação da pandemia e das necessidades de teletrabalho e ensino à
distância, continuam as nossas recomendações de software livre: [**COVID-19:
Software Educativo Livre para Ensino e
Teletrabalho**](https://adistancia.ansol.org).

Estes são apenas alguns exemplos do software que utilizamos e/ou recomendamos,
e a quem dirigimos o nosso muito grande <strong>OBRIGADO</strong>! Queremos
também agradecer a toda a comunidade de Software Livre no geral.

Encorajamo-vos a aproveitarem o dia de hoje para agradecerem a quem faz o
Software Livre que tanto adoram!

[ilovefs]: https://fsfe.org/activities/ilovefs/
[fsfe]: https://fsfe.org/
[20anos]: https://ansol.org/noticias/2021-09-12-ansol-20-anos-de-software-livre/
