---
categories:
- '2013'
metadata:
  event_location:
  - event_location_value: 'Fábrica de Santo Thyrso, Rua Dr. Oliveira Salazar, Nº 88,
      4780-453 Santo Tirso '
  event_site:
  - event_site_url: http://python.pt/eventos/2013/09/07/encontro-tecnico-pythonpt-7-setembro/
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2013-09-07 09:30:00.000000000 +01:00
    event_start_value2: 2013-09-07 16:00:00.000000000 +01:00
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 32
  node_id: 205
layout: evento
title: Encontro Técnico Python.pt 7 Setembro
created: 1378169967
date: 2013-09-03
---
<h2>Programa</h2><p>10:30 - Encontro na estação de São Bento (partida às 11:00) <br>12:00 - Tour às instalações da Fábrica de Santo Thyrso <br>13:00 - Almoço <br>14:00 - "Criação de Aplicações GUI com PyQt" - Rizo Isrof (@rizo_isrof) <br>15:00 - "Desenvolvimento Orientado a Testes em Django e Integração Contínua com Jenkins CI" - José Durães (@joseduraes) <br>16:00 - a confirmar </p>
