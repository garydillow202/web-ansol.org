---
categories: []
metadata:
  event_start:
  - event_start_value: 2015-05-06 23:00:00.000000000 +01:00
    event_start_value2: 2015-05-06 23:00:00.000000000 +01:00
  slide:
  - slide_value: 0
  node_id: 315
layout: evento
title: 'Conferência: Alterações recentes (e vindouras?) ao Código do Direito de Autor'
created: 1429828622
date: 2015-04-23
---
<p>Nuno Sousa e Silva (FDUCP Porto) e Tito Rendas (FDUCP Lisboa) farão uma análise crítica ao recente pacote legislativo e discutirão possíveis alterações futuras na área do Direito de Autor. Em cima da mesa estarão, entre outros, os seguintes temas:</p><p>&nbsp;</p><p>- Cópia privada;</p><p>- Gestão colectiva;</p><p>- Obras órfãs;</p><p>- Tutela dos direitos de autor na Internet;</p><p>- Agregação de notícias.</p><p>&nbsp;</p><p>O evento terá lugar em Lisboa, no dia 7 de Maio, às 18:00h, e no Porto, no dia 14 de Maio, às 18:00h, nos&nbsp;<em>campus&nbsp;</em>de Palma de Cima e da Foz&nbsp;da Universidade Católica Portuguesa, respectivamente.</p>
