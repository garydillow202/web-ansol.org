---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 340
  event:
    location: Universidade Portucalense, Porto
    site:
      title: ''
      url: http://www.bad.pt/noticia/2015/07/30/jornada-sobre-os-sistemas-de-informacao-em-open-source-em-setembro-no-porto/
    date:
      start: 2015-09-11 00:00:00.000000000 +01:00
      finish: 2015-09-11 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: 'Sistemas de informação em Open Source: partilha de experiências'
created: 1438354507
date: 2015-07-31
aliases:
- "/evento/340/"
- "/node/340/"
---

