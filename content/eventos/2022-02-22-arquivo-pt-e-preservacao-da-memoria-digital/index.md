---
layout: evento
title: O Arquivo.pt e a preservação da memória digital (1º Webinar)
metadata:
  event:
    date:
      start: 2022-02-22 15:00:00
      finish: 2022-02-22 16:30:00
    location: Online
    site:
      url: https://forms.gle/Q2sSKS2thWuS7AiC6
---

Mais informação na [página do programa](https://blog.wikimedia.pt/2022/02/08/ciclo-de-webinarios-em-colaboracao-com-o-arquivo-pt/).

## Património cultural na Web: como preservar as referências na Wikipédia?

> O objetivo deste ciclo de Webinars é apresentar os serviços do Arquivo.pt e
> disseminar a sua utilização para que o património histórico publicado na web
> possa ser preservado e explorado por qualquer cidadão.
>
> Este ciclo de Webinars, dedicado à preservação da memória cultural publicada na
> Web, é uma colaboração entre a Wikimedia Portugal e o Arquivo.pt (Fundação para
> a Ciência e a Tecnologia I.P.).


## O Arquivo.pt e a preservação da memória digital (1º Webinar)

> Webinar que motiva para a importância da preservação da memória digital e
> apresenta os serviços disponibilizados pelo Arquivo.pt que estão acessíveis a
> qualquer cidadão ou organização. Este webinar destina-se a qualquer utilizador
> da Internet. [Saber mais](https://sobre.arquivo.pt/pt/ajuda/formacao/modulo-a/)
