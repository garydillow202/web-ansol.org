---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 289
  event:
    location: Lisboa, Porto e Tomar
    site:
      title: ''
      url: https://day.arduino.cc
    date:
      start: 2015-03-28 00:00:00.000000000 +00:00
      finish: 2015-03-28 00:00:00.000000000 +00:00
    map: {}
layout: evento
title: Arduino Day 2015
created: 1425253293
date: 2015-03-01
aliases:
- "/evento/289/"
- "/node/289/"
---
<p>Mais uma vez se celebra o "Arduino Day" um pouco por todo o mundo, e Portugal não é excepção!</p><p>Em Tomar celebra-se no dia anterior (27) das 10:30 às 17:30 haverão workshops e apresentações de projectos.</p><p>Em Lisboa, o dia começa às 8:30 e termina às 14:00, com conversas sobre o BITalino, o FabLab e uma introdução à electrónica básica.</p><p>No Porto, o evento começa às 10:00 e termina às 17:00, e haverá espaço para Worshops Hands-On e demostração de projectos.</p><p>&nbsp;</p><p>O difícil é a escolher a qual ir!</p><p>&nbsp;</p><p>Lisboa: http://arduinoday.marcocarvalho.me/</p><p>Porto: http://www.cinel.pt/appv2/Eventos/Arduino-Day</p><p>Tomar: https://www.facebook.com/events/1778318625727051/</p>
