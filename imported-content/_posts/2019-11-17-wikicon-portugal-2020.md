---
categories: []
metadata:
  event_location:
  - event_location_value: Casa do Infante, Porto
  event_site:
  - event_site_url: https://meta.wikimedia.org/wiki/WikiCon_Portugal
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2020-01-17 00:00:00.000000000 +00:00
    event_start_value2: 2020-01-19 00:00:00.000000000 +00:00
  mapa:
  - mapa_geom: !binary |-
      AQEAAAAAAAAQwjohwD1g9RcikkRA
    mapa_geo_type: point
    mapa_lat: !ruby/object:BigDecimal 27:0.41141665453732e2
    mapa_lon: !ruby/object:BigDecimal 27:-0.8614761829376e1
    mapa_left: !ruby/object:BigDecimal 27:-0.8614761829376e1
    mapa_top: !ruby/object:BigDecimal 27:0.41141665453732e2
    mapa_right: !ruby/object:BigDecimal 27:-0.8614761829376e1
    mapa_bottom: !ruby/object:BigDecimal 27:0.41141665453732e2
    mapa_geohash: ez3f5cwgt6rczvxu
  slide:
  - slide_value: 0
  node_id: 714
layout: evento
title: WikiCon Portugal 2020
created: 1574017927
date: 2019-11-17
---
<p>Por ocasião dos 19 anos da Wikipédia e dos 10 anos da Wikimédia Portugal, o <strong>WikiCon Portugal</strong> 2020 quer celebrar a comunidade que tomou para si a missão de "cada ser humano poder partilhar livremente a soma de todo o conhecimento". O objectivo desta primeira edição do WikiCon Portugal é o desenvolvimento de capacidades da comunidade.</p><p>Entre outras apresentações, a ANSOL irá estar presente, e falar sobre "A Wikimedia e o dia do Domínio Público".</p>
