---
categories: []
metadata:
  event_location:
  - event_location_value: Campsite Belvedere, I-3469 Dudelange, Luxembourg
  event_site:
  - event_site_url: https://haxogreen.lu/
    event_site_title: Haxogreen
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2016-07-27 23:00:00.000000000 +01:00
    event_start_value2: 2016-07-30 23:00:00.000000000 +01:00
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 137
  - tags_tid: 138
  - tags_tid: 117
  node_id: 369
layout: evento
title: HaxoGreen
created: 1445735948
date: 2015-10-25
---
<h1>HaxoGreen</h1><p>... is a 4-day outdoor summer camp for over 100 hackers from all over Europe<br><br>... is where a community of creators, innovators, engineers, programmers and artists gathers and meets for an informal information-exchange, for talks, workshops and live music, and above all, for fun.<br><br>... is the summer-feeling of july, the adventure of outdoor camping, the smell of bbq and the thrill of long nights.<br><br>... is a cozy campsite with lots of technology</p>
