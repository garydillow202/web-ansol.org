---
categories:
- wikidata
- domínio público
metadata:
  event_location:
  - event_location_value: Online
  event_site:
  - event_site_url: https://pt.wikimedia.org/wiki/WikidataDays_Sessions_I
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2021-04-11 15:00:00.000000000 +01:00
    event_start_value2: 2021-04-11 18:00:00.000000000 +01:00
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 336
  - tags_tid: 313
  node_id: 783
layout: evento
title: WikidataDays Sessions I
created: 1616354106
date: 2021-03-21
---
<p>A primeira edição dos WikidataDays Sessions será completamente online e dedicada à cultura. Tendo como base de trabalho uma listagem dos autores Portugueses que entraram em domínio público a 1 de Janeiro deste ano, iremos atualizar os dados no Wikidata e na Wikipédia, corrigir a informação que necessite ser corrigida assim como analisar as fontes de informação que podem ser usadas. Teremos editores experientes do Wikidata, conhecedores dos meandros dos endpoints de dados livres da da Biblioteca Nacional e similares, biscoitos e muito boa disposição! Quer dizer, biscoitos não partilharemos por questões óbvias de saúde e privacidade, mas o resto está garantido, por isso não faltes!</p>
