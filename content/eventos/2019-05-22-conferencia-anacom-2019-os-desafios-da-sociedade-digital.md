---
categories: []
metadata:
  mapa:
  - mapa_geom: "\x01\a\0\0\0\0\0\0\0"
    mapa_geo_type: geometrycollection
    mapa_geohash: ''
  slide:
  - slide_value: 0
  node_id: 669
  event:
    location: 
    site:
      title: ''
      url: https://www.anacom.pt/render.jsp?contentId=1470931
    date:
      start: 2019-05-29 00:00:00.000000000 +01:00
      finish: 2019-05-29 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: 'Conferência ANACOM 2019: os desafios da Sociedade Digital'
created: 1558551008
date: 2019-05-22
aliases:
- "/evento/669/"
- "/node/669/"
---

