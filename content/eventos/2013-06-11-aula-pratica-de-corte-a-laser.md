---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 180
  event:
    location: Audiência Zero (Matosinhos)
    site:
      title: ''
      url: http://www.labcd.org/index.php?task=evt&id=98&option=com_lcd&Itemid=2
    date:
      start: 2013-07-09 20:00:00.000000000 +01:00
      finish: 2013-07-09 20:00:00.000000000 +01:00
    map: {}
layout: evento
title: Aula Prática de Corte a Laser
created: 1370972975
date: 2013-06-11
aliases:
- "/evento/180/"
- "/node/180/"
---
<p>A aula prática de corte a laser consistirá numa introdução ao LibreCad (programa open source de desenho assistido por computador) e de uma sessão prática de corte onde os participantes poderão cortar e gravar os modelos que desenharem durante a aula.</p>
