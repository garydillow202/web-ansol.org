---
categories: []
metadata:
  mapa:
  - mapa_geom: "\x01\a\0\0\0\0\0\0\0"
    mapa_geo_type: geometrycollection
    mapa_geohash: ''
  slide:
  - slide_value: 0
  node_id: 702
  event:
    location: Lisboa
    site:
      title: ''
      url: https://sobre.arquivo.pt/pt/wdpd2019/
    date:
      start: 2019-11-07 00:00:00.000000000 +00:00
      finish: 2019-11-07 00:00:00.000000000 +00:00
    map: {}
layout: evento
title: Dia Mundial da Preservação Digital
created: 1572193396
date: 2019-10-27
aliases:
- "/evento/702/"
- "/node/702/"
---

