---
categories: []
metadata:
  image:
  - image_fid: 64
    image_alt: ''
    image_title: ''
    image_width: 680
    image_height: 425
  node_id: 767
layout: article
title: 'Dia Internacional contra o DRM #DayAgainstDRM #IDAD'
created: 1607042680
date: 2020-12-04
aliases:
- "/article/767/"
- "/node/767/"
---
<p>Este ano, o Dia Internacional contra o DRM celebra-se a 4 de dezembro, com a Defective by Design, da Free Software Foundation, a lançar uma campanha focada nos serviços de streaming e na utilização injusta do DRM que estes serviços fazem.</p><p>A campanha é sobretudo dirigida à Netflix - se esta empresa abandonasse o uso de DRM, rapidamente as outras seguiriam - e tem como objetivo levar as pessoas a pedirem à Netflix para deixar de usar DRM e também que cancelem a sua subscrição em protesto pelo uso de DRM que a Netflix faz.<br><br>Em Portugal, a ANSOL teve um papel fulcral na correção da lei do DRM, sendo-nos agora permitido quebrar o DRM para fazer usos legais das obras (<a href="https://fsfe.org/news/2019/news-20191113-01.en.html" target="_blank">um artigo sobre como conseguimos aqui</a>). Mas muitos outros países ainda têm leis que não o permitem. Por outro lado, para além dos conteúdos, o DRM avança noutras áreas: automóveis, tratores, máquinas de café, e outros objetos que usamos no dia-a-dia.<br><br>Há várias formas de participar neste dia:<br><br></p><ul><li>Manifestarem-se nas redes sociais, grupos de chat, ou nos vossos blogs, usando a hashtag #DayAgainstDRM e #IDAD;</li><li>Partilhar os posts das várias entidades que estão a fazer esta campanha:</li></ul><p>ANSOL - <a href="https://twitter.com/ANSOL" target="_blank">https://twitter.com/ANSOL</a><br>FSF - <a href="https://twitter.com/fsf" target="_blank">https://twitter.com/fsf</a><br>Framasoft - <a href="https://twitter.com/framasoft" target="_blank">https://twitter.com/framasoft</a><br>EFF - <a href="https://twitter.com/EFF" target="_blank">https://twitter.com/EFF</a><br><br></p><ul><li>A FSF criou uma sala para uma sessão usando o BigBlueButton, para quem quiser participar: <a href="https://testgreenlight.fsf.org/gre-x6u-y69" target="_blank">https://testgreenlight.fsf.org/gre-x6u-y69</a></li><li>Fazer o desafio de passar um dia sem DRM e convencer os amigos a fazerem o mesmo. A Defective by Design tem um guia para viver sem DRM aqui: <a href="https://www.defectivebydesign.org/guide" target="_blank">https://www.defectivebydesign.org/guide</a></li></ul><p><strong>No site do evento, há mais formas de participar: <a href="https://www.defectivebydesign.org/dayagainstdrm" target="_blank">https://www.defectivebydesign.org/dayagainstdrm</a></strong><br><br>Por fim, se não usam Netflix ou se estão a pensar em cancelar a vossa subscrição, quer por usarem software livre, quer por preocupação com a falta de controlo do vosso computador ou dispositivo onde usam este serviço, façam saber isso mesmo à Netflix e peçam-lhes para abandonarem o DRM. No Twitter, a Netflix Portugal está em <a href="https://twitter.com/NetflixPT" target="_blank">https://twitter.com/NetflixPT</a>.<a href="https://help.netflix.com/en/contactus" target="_blank"><br></a></p><p><strong>Para saber mais sobre o DRM e os problemas para os cidadãos, a ANSOL tem um <a href="https://drm-pt.info/" target="_blank">site dedicado ao tema aqui</a>, em português.</strong></p>
