---
categories:
- hackforgood hackathon gulbenkian
metadata:
  event_location:
  - event_location_value: Palácio dos Correios - Porto
  event_site:
  - event_site_url: https://gulbenkian.pt/hackforgood/2018-edition/hackathon/
    event_site_title: Hackforgood - Hackathon
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2018-05-04 23:00:00.000000000 +01:00
    event_start_value2: 2018-05-05 23:00:00.000000000 +01:00
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 219
  node_id: 552
layout: evento
title: Hackforgood 2018 - Hackathon
created: 1520625182
date: 2018-03-09
---
<div class="small-12 medium-8 large-10 columns fadeInUp wow" style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInUp;" data-wow-delay="0.2s"><h1>Hack For Good</h1></div><p>Hack for Good is a 2 days non-stop development&nbsp;marathon organized by Calouste Gulbenkian Foundation to foster the development of technological solutions to social challenges.</p><p>Programmers, designers and entrepreneurs work together to develop apps, websites, platforms or even prototypes that translate into true innovative and scalable solutions for real problems, in an event that also wishes to strengthen the connection between the technological sector (and its professionals) and the social sector.</p><p>Gulbenkian Foundation believes that technology is a potentiator of innovative solutions to the main social worldwide and nationwide problems. Due to the big diversity of these problems, a new topic is explored in each edition.</p><p>The 2 last editions generated more than 600 registrations, translated into 308 participants and 69 projects.</p><p>Are you ready to Hack For Good?</p>
