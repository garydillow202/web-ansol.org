---
categories: []
metadata:
  node_id: 140
  event:
    location: Entre Nós, Lisboa
    site:
      title: ''
      url: http://loco.ubuntu.com/events/ubuntu-pt/2363-ubuntu-1310-release-party/
    date:
      start: 2013-04-26 19:00:00.000000000 +01:00
      finish: 2013-04-26 19:00:00.000000000 +01:00
    map: {}
layout: evento
title: Ubuntu 13.04 Release Party
created: 1366292488
date: 2013-04-18
aliases:
- "/evento/140/"
- "/node/140/"
---
<p>Mais uma vez a comunidade Ubuntu Portugal vai-se juntar para celebrar mais um lançamento do Ubuntu. Este convívio servirá para partilhar experiências com outros utilizadores, tirar dúvidas e até para dar uma mãozinha a quem precisar de ajuda com as instalações.</p>
