---
categories: []
metadata:
  event_site:
  - event_site_url: https://www.esop.pt/destaque/workshop-open-source-transformacao-digital-1asessao-2019
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2019-10-21 23:00:00.000000000 +01:00
    event_start_value2: 2019-10-21 23:00:00.000000000 +01:00
  mapa:
  - mapa_geom: "\x01\a\0\0\0\0\0\0\0"
    mapa_geo_type: geometrycollection
    mapa_geohash: ''
  slide:
  - slide_value: 0
  node_id: 694
layout: evento
title: UX/UI & plataformas de Experiência Digital
created: 1567633996
date: 2019-09-04
---
<div class="clearfix text-formatted field field--name-field-cc-text-columns field--type-text-long field--label-hidden field__items"><div class="field__item"><p>A ESOP vai dar inicio a mais um Ciclo de workshops organizado em parceria com a AMA e TicApp.<br> A 1ª sessão de 2019 vai ser dedicada ao tema "UX/UI &amp; plataformas de Experiência Digital" e acontecerá a 22 de Outubro, no grande auditório do Laboratório Nacional de Engenharia Civil em Lisboa.</p></div></div>
