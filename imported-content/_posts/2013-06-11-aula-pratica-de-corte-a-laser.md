---
categories: []
metadata:
  event_location:
  - event_location_value: Audiência Zero (Matosinhos)
  event_site:
  - event_site_url: http://www.labcd.org/index.php?task=evt&id=98&option=com_lcd&Itemid=2
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2013-07-09 19:00:00.000000000 +01:00
    event_start_value2: 2013-07-09 19:00:00.000000000 +01:00
  slide:
  - slide_value: 0
  node_id: 180
layout: evento
title: Aula Prática de Corte a Laser
created: 1370972975
date: 2013-06-11
---
<p>A aula prática de corte a laser consistirá numa introdução ao LibreCad (programa open source de desenho assistido por computador) e de uma sessão prática de corte onde os participantes poderão cortar e gravar os modelos que desenharem durante a aula.</p>
